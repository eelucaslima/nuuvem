# README

## Ruby version
2.5.1

## Install gems
bundle install

## To load database schema
rails db:schema:load

## To start the application
bundle exec bin/rails s

## To reset database, run the commads:
rails db:drop
rails db:schema:load
