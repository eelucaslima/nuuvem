# frozen_string_literal: true

class Sale < ApplicationRecord
  before_save :normalize_item_price, if: :item_price

  validates :id, uniqueness: true

  private

  def normalize_item_price
    self.item_price = item_price.round(2)
  end
end
