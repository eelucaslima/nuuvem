# frozen_string_literal: true

require 'csv'

module Services
  class CSVParser
    CSV_OPTIONS = {
      col_sep: "\t",
      headers: true,
      header_converters: ->(header) { header.tr(' ', '_') }
    }.freeze

    def initialize(file:)
      @file = file
      @data_parsed = []
    end

    def call
      CSV.foreach(@file, CSV_OPTIONS).each do |row|
        @data_parsed.append(row.to_h)
      end

      @data_parsed
    end
  end
end
