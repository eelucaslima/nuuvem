# frozen_string_literal: true

module Forms
  class Sale
    def initialize(sales:)
      @sales = sales
    end

    def save
      ActiveRecord::Base.transaction do
        ::Sale.create!(@sales)
      end

      true
    rescue StandardError
      false
    end

    private

    def sale_id?
      @sales.each do |sale|
        exit if sale[:id].present?
      end

      false
    end
  end
end
