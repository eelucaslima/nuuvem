# frozen_string_literal: true

class SalesController < ApplicationController
  def index
    @total_gross_income = Sale.sum('item_price * purchase_count')
  end

  def new
    @sale = Sale.new
  end

  def create
    sales = Services::CSVParser.new(file: params[:sales][:file]).call

    sale_form = Forms::Sale.new(sales: sales)

    if sale_form.save
      redirect_to sales_path, format: :html
    else
      redirect_to new_sale_url, format: :html
    end
  end
end
