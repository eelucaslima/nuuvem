# frozen_string_literal: true

ActiveSupport::Dependencies.autoload_paths.delete(Rails.root.join("app", "services").to_s)
ActiveSupport::Dependencies.autoload_paths.delete(Rails.root.join("app", "forms").to_s)

