# frozen_string_literal: true

require 'rails_helper'

RSpec.describe Services::CSVParser do
  let(:data_parsed) do
    [
      {
        'purchaser_name' => 'João Silva',
        'item_description' => 'R$10 off R$20 of food',
        'item_price' => '10.0',
        'purchase_count' => '2',
        'merchant_address' => '987 Fake St',
        'merchant_name' => "Bob's Pizza"
      },
      {
        'purchaser_name' => 'Amy Pond',
        'item_description' => 'R$30 of awesome for R$10',
        'item_price' => '10.0',
        'purchase_count' => '5',
        'merchant_address' => '456 Unreal Rd',
        'merchant_name' => "Tom's Awesome Shop"
      }
    ]
  end

  it 'parses csv file' do
    file = file_fixture('sales/example.tab')

    service = Services::CSVParser.new(file: file)

    expect(service.call).to eq data_parsed
  end
end
