# frozen_string_literal: true

require 'rails_helper'

RSpec.describe SalesController, type: :controller do
  describe 'POST #create' do
    let(:file) { file_fixture('sales/example.tab') }

    it 'creates two sales' do
      expect do
        post :create, params: { sales: { file: file } }
      end.to change { Sale.count }.by(2)
    end
  end
end
