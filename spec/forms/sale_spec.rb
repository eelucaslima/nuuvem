# frozen_string_literal: true

require 'rails_helper'

RSpec.describe Forms::Sale do
  context 'with valid argument' do
    let(:csv_parsed) { Services::CSVParser.new(file: file_fixture('sales/example.tab')).call }
    let(:sale_form) { described_class.new(sales: csv_parsed) }

    it { expect { sale_form.save }.to change { ::Sale.count }.by(2) }

    it 'retuns true' do
      expect(sale_form.save).to be true
    end
  end

  context 'with integer argument' do
    let(:sale_form) { described_class.new(sales: 123) }

    it { expect { sale_form.save }.to_not change { ::Sale.count }.from(0) }

    it 'returns false' do
      expect(sale_form.save).to be false
    end
  end

  context 'with invalid hash key' do
    let(:sale_form) { described_class.new(sales: [{ 'invalid_key': 'test' }]) }

    it { expect { sale_form.save }.to_not change { ::Sale.count }.from(0) }

    it 'returns false' do
      expect(sale_form.save).to be false
    end
  end

  context 'with invalid record' do
    it 'returns false' do
      sale_form = described_class.new(sales: [{}, { id: '1' }])

      expect(sale_form.save).to be false
    end
  end
end
